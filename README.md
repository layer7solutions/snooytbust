# SnooYTBust
SnooYTBust scans a subreddit for newly posted YouTube links. If it matches a blacklist, the post or comment is removed.

If you define a bot token & notification channel, the bot will also announce removed items in Slack.
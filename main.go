package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/glaslos/tlsh"
	"github.com/go-pg/pg/v9"
	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"github.com/ttgmpsn/mira"
	miramodels "github.com/ttgmpsn/mira/models"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"
)

type botConfig struct {
	LogFile   string
	Subreddit string

	Slack struct {
		BotUserToken        string
		NotificationChannel string
	}

	Database struct {
		Username string
		Password string
		Host     string
		Database string
	}

	Reddit mira.Credentials

	Services struct {
		YTAPIKey string
	}
}

var config botConfig
var db *pg.DB

var slackAPI *slack.Client
var slackInfo *slack.AuthTestResponse

var reddit *mira.Reddit

// According to Hawkmoona, Gjallarhorn Day is the Friday closest to the August 14th.
var gjallyDay time.Time = time.Date(2024, time.August, 16, 12, 0, 0, 0, time.UTC)

func init() {
	logrus.SetFormatter(&prefixed.TextFormatter{})
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(os.Stdout)
}

var log = logrus.WithField("prefix", "main")

func main() {
	var logfile, conffile string
	flag.StringVar(&logfile, "log", "", "path to a logfile (optional, stdout & debug enabled when skipped)")
	flag.StringVar(&conffile, "conf", "", "path to a configfile (required)")
	flag.Parse()

	if logfile != "" {
		f, err := os.OpenFile(logfile, os.O_TRUNC|os.O_WRONLY|os.O_CREATE, 0660)
		if err != nil {
			panic("error opening log file")
		}
		defer f.Close()
		logrus.SetOutput(f)
		logrus.SetLevel(logrus.InfoLevel)
	}

	var err error

	log.Info("Starting SnooYTBust (DTG)")
	log.Info("Reading config")
	if len(conffile) == 0 {
		log.Fatal("Please set the conffile flag")
	}
	if _, err = toml.DecodeFile(conffile, &config); err != nil {
		log.WithError(err).WithField("file", conffile).Fatal("reading config")
	}

	// Connect to DB
	db = pg.Connect(&pg.Options{
		User:     config.Database.Username,
		Password: config.Database.Password,
		Database: config.Database.Database,
		Addr:     config.Database.Host,
	})

	// listen to Slack
	if len(config.Slack.NotificationChannel) > 0 {
		slackAPI = slack.New(
			config.Slack.BotUserToken,
			slack.OptionDebug(false),
		)

		slackInfo, err = slackAPI.AuthTest()
		if err != nil {
			log.WithError(err).Fatal("Slack Auth failed")
		}
		log.Infof("Connected to Slack as %s (ID %s)", slackInfo.User, slackInfo.UserID)
	}

	// Login to Reddit
	reddit = mira.Init(config.Reddit)
	err = reddit.LoginAuth()
	if err != nil {
		log.WithError(err).Fatal("Reddit Auth failed")
	}
	rMeObj, err := reddit.Me().Info()
	if err != nil {
		log.WithError(err).Fatal("Reddit Auth failed")
	}
	rMe, _ := rMeObj.(*miramodels.Me)
	log.WithField("prefix", "reddit").Infof("Connected to Reddit as /u/%s", rMe.Name)

	// Login to YouTube
	ctx := context.Background()
	ytService, err := youtube.NewService(ctx, option.WithAPIKey(config.Services.YTAPIKey))
	if err != nil {
		log.WithError(err).Fatal("YouTube Auth failed")
	}

	pS, err := reddit.Subreddit(config.Subreddit).StreamPosts()
	if err != nil {
		log.WithError(err).Fatal("Could not create post stream")
	}
	jpS, err := reddit.Subreddit("destinycirclejerk").StreamPosts()
	if err != nil {
		log.WithError(err).Fatal("Could not create post stream")
	}
	cS, err := reddit.Subreddit(config.Subreddit).StreamComments()
	if err != nil {
		log.WithError(err).Fatal("Could not create comment stream")
	}
	type ujHash struct {
		ID   string
		TLSH string
		Hash *tlsh.Tlsh
	}
	ujHashes := []ujHash{}
	_, err = db.Query(&ujHashes, "SELECT id, tlsh FROM dtg_unjerk")
	if err != nil && err != pg.ErrNoRows {
		log.WithError(err).Fatal("DB query failed")
	}

	for i, h := range ujHashes {
		hash, err := tlsh.ParseStringToTlsh(h.TLSH)
		if err != nil {
			log.WithError(err).Fatal("TLSH error")
			continue
		}
		ujHashes[i].ID = strings.TrimSpace(h.ID)
		ujHashes[i].Hash = hash
	}

	gjallyDayChecked := make(map[string]bool)

	for {
		var s miramodels.Submission
		var pSC, cSC, jpSC bool
		select {
		case s = <-pS.C:
			if s == nil {
				pSC = true
			}
		case s = <-cS.C:
			if s == nil {
				cSC = true
			}
		case s = <-jpS.C:
			if s == nil {
				jpSC = true
			}

		}
		if s == nil {
			log.Warn("looks like one channel is closed?")
			if pSC {
				log.Info("post channel was closed, recreating")
				chanCreated := false
				for !chanCreated {
					pS, err = reddit.Subreddit(config.Subreddit).StreamPosts()
					if err != nil {
						log.WithError(err).Warn("Could not create post stream, trying again in a minute (reddit down?)...")
						time.Sleep(1 * time.Minute)
						continue
					}
					chanCreated = true
				}
			}
			if jpSC {
				log.Info("DCJ post channel was closed, recreating")
				chanCreated := false
				for !chanCreated {
					jpS, err = reddit.Subreddit("destinycirclejerk").StreamPosts()
					if err != nil {
						log.WithError(err).Warn("Could not create post stream, trying again in a minute (reddit down?)...")
						time.Sleep(1 * time.Minute)
						continue
					}
					chanCreated = true
				}
			}
			if cSC {
				log.Info("comment channel was closed, recreating")
				chanCreated := false
				for !chanCreated {
					cS, err = reddit.Subreddit(config.Subreddit).StreamComments()
					if err != nil {
						log.WithError(err).Warn("Could not create comment stream, trying again in a minute (reddit down?)...")
						time.Sleep(1 * time.Minute)
						continue
					}
					chanCreated = true
				}
			}
			continue
		}
		body := s.GetBody()
		l := log.WithField("thing_id", s.GetID())

		if s.GetAuthor() == rMe.Name {
			// Exempts bot posts, namely TWABs with MotW from blacklisted channels
			continue
		}

		// UNJERK START //
		l = l.WithField("prefix", "unjerk")
		if s.GetSubreddit() == "destinycirclejerk" {
			if len(body) < 50 {
				l.Debug("post body too short, not handling")
				continue
			}
			hash, err := tlsh.HashBytes([]byte(body))
			if err != nil {
				l.WithError(err).Error("TLSH error")
				continue
			}
			if _, err = db.Exec(`INSERT INTO "dtg_unjerk" ("id", "tlsh") VALUES (?, ?);`, s.GetID(), hash.String()); err != nil {
				pgErr, ok := err.(pg.Error)
				if !ok || !pgErr.IntegrityViolation() {
					l.WithError(err).Warn("insert hash to DB failed")
				}
				l.Debug("hash alrady in DB")
				continue
			}
			ujHashes = append(ujHashes, ujHash{
				ID:   string(s.GetID()),
				TLSH: hash.String(),
				Hash: hash,
			})
			l.Debugf("inserted hash %s to DB", hash)
			continue
		}
		if len(body) > 50 {
			hash, err := tlsh.HashBytes([]byte(body))
			if err != nil {
				l.WithError(err).Error("TLSH error")
			} else {
				for _, h := range ujHashes {
					score := hash.Diff(h.Hash)
					if score > 85 {
						continue
					}
					l.Infof("Similar to DCJ (ID %s, Score %d), reporting", h.ID, score)
					// _, err = reddit.MiraRequest("POST", mira.RedditOauth+"/api/report", map[string]string{
					// 	"thing_id": string(s.GetID()),
					// 	"reason":   fmt.Sprintf("Post very similar to DCJ post https://redd.it/%s (Score: %d)", h.ID[3:], score),
					// 	"api_type": "json",
					// })
					// if err != nil {
					// 	l.WithError(err).Error("reporting failed")
					// }
				}
			}
		}

		// GJALLYDAY START //
		l = l.WithField("prefix", "gjally")
		if s.CreatedAt().After(gjallyDay) && s.CreatedAt().Before(gjallyDay.AddDate(0, 0, 1)) && s.GetID().Type() == miramodels.KPost && !s.IsApproved() && !s.IsRemoved() {
			l.Debugf("gjally day check start for %s", s.GetURL())
			good, ok := gjallyDayChecked[s.GetAuthor()]
			if !ok {
				good = false
				successful := true
				last := miramodels.RedditID("")
			out:
				for {
					usersubs, err := reddit.Redditor(s.GetAuthor()).SubmissionsAfter(last, 100)
					if err != nil {
						l.WithError(err).Error("could not grab submissions")
						good = true
					}
					if len(usersubs) == 0 {
						l.Debug("reached end of submissions, breaking")
						successful = false
						break
					}
					for _, usersub := range usersubs {
						last = usersub.GetID()
						l.Debugf("checking %s (%s)", last, usersub.GetURL())
						if !usersub.CreatedAt().Before(time.Now().AddDate(0, 0, -7)) {
							l.Debugf("too new (%s), skipping", usersub.CreatedAt())
							continue
						}

						if usersub.CreatedAt().Before(time.Now().AddDate(0, 0, -300)) {
							l.Debugf("too old (%s), breaking", usersub.CreatedAt())
							break out
						}
						if usersub.GetSubreddit() == "DestinyTheGame" && usersub.GetScore() > 1 {
							l.Infof("Good user due to submission %s, breaking", last)
							good = true
							break out
						}
					}
				}

				if successful {
					gjallyDayChecked[s.GetAuthor()] = good
				}
			}
			if !good {
				l.Infof("Removing, not eglible to post during gjally day")
				reddit.Post(string(s.GetID())).Remove(false)
				if len(config.Slack.NotificationChannel) > 0 {
					slackAPI.PostMessage(config.Slack.NotificationChannel, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
						slack.MsgOptionText(
							fmt.Sprintf("*Attention:* _The following item has been removed._\nThe User is not eglible to post during Gjallarhorn day.\n*Reddit Author:* %s\n*Subreddit:* /r/%s\n*Reddit Link:* %s",
								s.GetAuthor(), s.GetSubreddit(), s.GetURL(),
							),
							false,
						),
					)
				}
			}
		}

		// YTBUST START //
		l = l.WithField("prefix", "ytbust")
		links := extractYT(s.GetBody())
		if len(links) == 0 {
			l.Debug("no video")
		}
		for _, link := range links {
			l = l.WithField("video_id", link)
			l.Debug("found YT link")
			call := ytService.Videos.List([]string{"id", "snippet"}).Id(link).Fields("items(id,snippet(channelId,channelTitle))")
			resp, err := call.Do()
			if err != nil {
				l.WithError(err).Error("YT API Error")
				continue
			}

			if len(resp.Items) < 1 {
				// No item found
				continue
			}

			for _, i := range resp.Items {
				l.Infof("YT video found - Channel: %s (ID %s)", i.Snippet.ChannelTitle, i.Snippet.ChannelId)

				var res struct {
					ID uint64
				}
				r, err := db.QueryOne(&res, "SELECT id FROM dtg_blacklist WHERE media_channel_id = ? AND media_platform_id = 1 LIMIT 1", i.Snippet.ChannelId)
				if err != nil && err != pg.ErrNoRows {
					l.WithError(err).Error("DB query failed")
					continue
				}
				if err == pg.ErrNoRows || r.RowsReturned() == 0 {
					continue
				}
				l.Infof("Blacklisted (ID %d), removing", res.ID)

				switch s.GetID().Type() {
				case miramodels.KPost:
					reddit.Post(string(s.GetID())).Remove(false)
				case miramodels.KComment:
					reddit.Comment(string(s.GetID())).Remove(false)
				}

				if len(config.Slack.NotificationChannel) > 0 {
					slackAPI.PostMessage(config.Slack.NotificationChannel, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
						slack.MsgOptionText(
							fmt.Sprintf("*Attention:* _The following item has been removed._\n*Channel Author:* %s\n*Reddit Author:* %s\n*Subreddit:* /r/%s\n*Reddit Link:* %s",
								resp.Items[0].Snippet.ChannelTitle, s.GetAuthor(), s.GetSubreddit(), s.GetURL(),
							),
							false,
						),
					)
				}
			}
		}
	}
}
